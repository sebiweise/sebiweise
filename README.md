### Hi there 👋, I´m Sebastian

- 🔭 I’m currently working on a HomeAssistant integration for [Parcello](https://www.parcello.org/)
- 🌱 I’m currently learning **Flutter, SwiftUI, Python**
- 👯 I’m looking to collaborate with **other content creators**

### Languages and Tools:

- Next.js
- React
- Angular
- Vue
- Dart
- Firebase
- Flutter
- Git
- ASP.NET (Framework) MVC/Core
- Linux
- Windows
- MySQL
- MSSQL

<!--
**sebiweise/sebiweise** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
